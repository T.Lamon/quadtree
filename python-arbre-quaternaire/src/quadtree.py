from __future__ import annotations
import json
import tkinter as tk


class QuadTree:
    NB_NODES: int = 4

    # gère la créa d'un noeud
    def __init__(self, hg: bool | QuadTree, hd: bool | QuadTree, bd: bool | QuadTree, bg: bool | QuadTree):
        self.hg = hg
        self.hd = hd
        self.bd = bd
        self.bg = bg
        pass

    @property
    def depth(self) -> int:
        """Récursion de la profondeur du QuadTree"""

        # on ignore quelle est la profondeur totale du QuadTree
        max_depth = 0

        # pour noeud dans le tableau
        for sous_arbre in [self.hg, self.hd, self.bd, self.bg]:
            # si un noeud est une instance de QuadTree
            if isinstance(sous_arbre, QuadTree):
                depth_sous_arbre = sous_arbre.depth  # on assigne la valeur de sa profondeur à une var
                # si la valeur de la var est supérieure à celle de max_depth
                if depth_sous_arbre > max_depth:
                    max_depth = depth_sous_arbre  # max_depth prend la valeur de la var et on l'assigne à max_depth

        return max_depth + 1  # on retourne la valeur de max_depth+1

    @staticmethod
    def fromFile(filename: str) -> QuadTree:  # on commence par remplir cette fonction
        """ Extrait le QuadTree du fichier .txt"""

        # le bloc "with" permet d'assurer la fermeture du fichier après usage,
        # on ouvre le fichier
        with open(filename, "r") as file:
            # lis le contenu du fichier
            content = file.read()
            # extrait le contenu du fichier à partir du premier caractère '['.
            # facultative si le fichier contient uniquement la structure JSON
            content = content[content.find('['):]
            # json charge le contenu du fichier en tant que structure de données Python.
            data_list = json.loads(content)
            # un appel à une méthode statique se fait toujours selon le modèle : [classe].[méthode]
            return QuadTree.fromList(data_list)

    @staticmethod
    def fromList(data: list) -> QuadTree:
        """Gère les comportements selon si les entrées de la liste du fichier .txt sont des listes ou pas"""

        # si la première entrée de data est une liste:
        if isinstance(data[0], list):
            hg = QuadTree.fromList(data[0])  # la partie haute-gauche du noeud est une instance de QuadTree
        # dans le cas contraire
        else:
            hg = data[0]  # la partie haute-gauche du noeud prend une valeur booléenne

        # si la deuxième entrée de data est une liste:
        if isinstance(data[1], list):
            hd = QuadTree.fromList(data[1])  # la partie haute-droite du noeud est une instance de QuadTree
        # dans le cas contraire
        else:
            hd = data[1]  # la partie haute-droite du noeud prend une valeur booléenne

        # si la troisième entrée de data est une liste:
        if isinstance(data[2], list):
            bd = QuadTree.fromList(data[2])  # la partie basse-droite du noeud est une instance de QuadTree
        # dans le cas contraire
        else:
            bd = data[2]  # la partie basse-droite du noeud prend une valeur booléenne

        # si la dernière entrée de data est une liste:
        if isinstance(data[3], list):
            bg = QuadTree.fromList(data[3])  # la partie basse-gauche du noeud est une instance de QuadTree
        # dans le cas contraire
        else:
            bg = data[3]  # la partie basse-gauche du noeud prend une valeur booléenne

        return QuadTree(hg, hd, bd, bg)


class TkQuadTree:
    SCREEN_WIDTH = 1024

    def __init__(self, quadtree):
        self.quadtree = quadtree

    def paint(self):
        """ TK representation of a Quadtree"""

        # on créer une fenètre aux dimensions indiquées par SCREEN_WIDTH
        window = tk.Tk()
        window.geometry(f"{self.SCREEN_WIDTH}x{self.SCREEN_WIDTH}")
        canvas = tk.Canvas(window, width=self.SCREEN_WIDTH, height=self.SCREEN_WIDTH)

        # pour chaque entrée de la liste du fichier .txt
        for _ in range(self.quadtree.depth):
            """
            TODO : faire la récursion
            si l'entrée est une liste, ce qui est important est son emplacement et sa largeure

            paint(x,y,w,quadtree)
            """
            w = self.SCREEN_WIDTH // 2
            h = self.SCREEN_WIDTH // 2

            # si l'entrée n'est pas une instance de hg d'un QuadTree
            if not isinstance(self.quadtree.hg, QuadTree):
                color = "blue" if self.quadtree.hg == 1 else "green"
                # créer un carré de couleur bleue ou verte selon la valeur
                canvas.create_rectangle(0, 0, w, h, fill=color)
                canvas.pack()
            else:
                # sinon créer un carré de couleur rouge
                canvas.create_rectangle(0, 0, w, h, fill="red")
                canvas.pack()

            if not isinstance(self.quadtree.hd, QuadTree):
                color = "blue" if self.quadtree.hd == 1 else "green"
                canvas.create_rectangle(w, 0, 2 * w, h, fill=color)
                canvas.pack()
            else:
                canvas.create_rectangle(w, 0, 2 * w, h, fill="red")
                canvas.pack()

            if not isinstance(self.quadtree.bd, QuadTree):
                color = "blue" if self.quadtree.bd == 1 else "green"
                canvas.create_rectangle(w, h, 2 * w, 2 * h, fill=color)
                canvas.pack()
            else:
                canvas.create_rectangle(w, h, 2 * w, 2 * h, )
                canvas.pack()

            if not isinstance(self.quadtree.bg, QuadTree):
                color = "blue" if self.quadtree.bg == 1 else "green"
                canvas.create_rectangle(0, h, w, 2 * h, fill=color)
                canvas.pack()
            else:
                canvas.create_rectangle(0, h, w, 2 * h, fill="red")
                canvas.pack()

        window.mainloop()
